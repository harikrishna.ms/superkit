import React from 'react';
import { AppProvider, Button} from '@shopify/polaris';
import "@shopify/polaris/build/esm/styles.css";

function SuperButton (props) {
  return(
  <AppProvider>
    <Button {...props}/>
  </AppProvider>
  )
}

export {SuperButton};